$( document ).ready(function() {

    $('.form .section__box .section__block').first().addClass('open').animate({ opacity : 1 }, 200);

    $('.button').click(function() {
        if ($(this).closest('.section__block').hasClass('open')) {
            $(this).closest('.section__block').removeClass('open').animate({ opacity : 0 }, 200);
            $(this).closest('.section__box').next().find('.section__block').addClass('open').animate({ opacity : 1 }, 200);
        } else {
            $('.section__block').removeClass('open').animate({ opacity : 0 }, 200);
        }
        return false;
    });

    $('.title__link').click(function() {
        if ($(this).closest('.section__box').find('.section__block').hasClass('open')) {
            $(this).closest('.section__box').find('.section__block').removeClass('open').animate({ opacity : 0 }, 200);
        } else {
            $('.section__block').removeClass('open').animate({ opacity : 0 }, 200);
            $(this).closest('.section__box').find('.section__block').addClass('open').animate({ opacity : 1 }, 200);
        }
        return false;
    });

    $('.checkbox').change(function(){
        var c = this.checked ? '#fffdef' : '#ffffff';
        $(this).closest('.section__item').css('background-color', c);
    });

    $(function() {
        $('[data-popup-open]').click(function() {
            var targeted_popup_class = jQuery(this).attr('data-popup-open');
            $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
            return false;
        });
        $('[data-popup-close]').on('click', function()  {
            var targeted_popup_class = jQuery(this).attr('data-popup-close');
            $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
            return false;
        });
    });

    $('a[href^="#section1"]').click( function() {
        $('html, body').animate({
            scrollTop: $('#section1').offset().top
        }, 1000);
    });



});

